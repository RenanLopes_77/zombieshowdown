﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SplitScreen : MonoBehaviour {

	public Image pressEnter;
	float delayDeltaTime = 0;
	float deltaInterval = 2.0f;
	bool enterPressed = false;

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown (KeyCode.Return)) {
			enterPressed = true;
			pressEnter.GetComponent<Animator> ().SetTrigger ("Pressed");
		}

		if (enterPressed) {
			delayDeltaTime += Time.deltaTime;

			if (delayDeltaTime >= deltaInterval) {
				NewGame ();
			}
		}
	}

	public void NewGame (){
		
		SceneManager.LoadScene ("Game");
	}
}
