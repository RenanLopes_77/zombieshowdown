﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ZombieManager : MonoBehaviour {

	public GameObject zombie;
	public GameObject spawnOne;
	public GameObject spawnTwo;
	public GameObject spawnThree;
	public GameObject spawnFour;

	Transform player;

	public Text texto;

	public int wave = 0;
	public int deadZombie = 0;
	float waveInterval = 1.0f;
	float waveDeltaTime = 0;

	public bool newWave = false;

	// Use this for initialization
	void Start () {
		player = GameObject.FindObjectOfType<PlayerController> ().transform;
	}
	
	// Update is called once per frame
	void Update () {

		texto.text = deadZombie.ToString ();

		waveDeltaTime += Time.deltaTime;
		if (waveDeltaTime >= waveInterval) {
			waveDeltaTime = 0;
			SpawnZombie ();
		}
			
	}

	void SpawnZombie (){
		int spawn = 1;

		wave++;
		waveInterval = wave + 1;

		for (int i = 0; i < wave * 2; i++){
			if (spawn == 1){
				GameObject z = Instantiate (zombie, spawnOne.transform);
				z.GetComponent<ZombieControl> ().setTarget (player);
				z.GetComponent<ZombieControl> ().setZm (GetComponent<ZombieManager> ());
				spawn++;
			}
			else if (spawn == 2){
				GameObject z = Instantiate (zombie, spawnTwo.transform);
				z.GetComponent<ZombieControl> ().setTarget (player);
				z.GetComponent<ZombieControl> ().setZm (GetComponent<ZombieManager> ());
				spawn++;
			}
			else if (spawn == 3){
				GameObject z = Instantiate (zombie, spawnThree.transform);
				z.GetComponent<ZombieControl> ().setTarget (player);
				z.GetComponent<ZombieControl> ().setZm (GetComponent<ZombieManager> ());
				spawn++;
			}
			else if (spawn == 4){
				GameObject z = Instantiate (zombie, spawnFour.transform);
				z.GetComponent<ZombieControl> ().setTarget (player);
				z.GetComponent<ZombieControl> ().setZm (GetComponent<ZombieManager> ());
				spawn = 1;
			}
		}
	}

	public void DeadZombie (){
		deadZombie++;
		//sb.setScore (deadZombie);
	}
}
