﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieControl : MonoBehaviour {

	Transform target;
	public float chaseRange;
	public float speed;
	ZombieManager zm;


	// Use this for initialization
	void Start () {
		speed = Random.Range (1, 10);

	}
	
	// Update is called once per frame
	void Update () {
		float distanceToTarget = Vector3.Distance (transform.position, target.position);

		if (distanceToTarget < chaseRange) {

			Vector3 targetDir = transform.position - target.position;

			float angle = Mathf.Atan2 (targetDir.y, targetDir.x) * Mathf.Rad2Deg - 90f;

			Quaternion q = Quaternion.AngleAxis (angle, Vector3.forward);

			transform.rotation = Quaternion.RotateTowards (transform.rotation, q, -15);

			transform.Translate (Vector3.up * Time.deltaTime * speed);
		}
	}

	public void setTarget (Transform t){
		target = t;
	}

	public void setZm (ZombieManager _zm){
		zm = _zm;
	}

	void OnTriggerEnter2D (Collider2D other){
		if (other.name.Contains ("Bullet")) {
			zm.DeadZombie ();
			Destroy (gameObject);
		}
	}
}
