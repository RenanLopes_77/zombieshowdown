﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {

	public GameObject Bullet;
	public BulletControl bc;
	public Image [] img = new Image[5];

	public float speed = 10;
	public int side = 1;

	int life = 1000;
	float horizontal;
	float vertical;
	bool facingUp = true;
	bool facingRight = true;

	Animator anim;

	void Start () {
		anim = GetComponent <Animator> ();

	}
	

	void Update () {
		horizontal = Input.GetAxis ("Horizontal");
		vertical = Input.GetAxis ("Vertical");

		anim.SetFloat ("Horizontal", Mathf.Abs (horizontal));
		anim.SetFloat ("Vertical", Mathf.Abs (vertical));

		transform.Translate (horizontal * Time.deltaTime * speed, vertical * Time.deltaTime * speed, 0);

		if (Input.GetKeyDown (KeyCode.Space)) {
			if (horizontal > -0.1 && horizontal < 0.1) {
				if (vertical > -0.1 && vertical < 0.1) {
					Instantiate (Bullet, transform);
				}
			}
		}

		if (vertical > 0) {
			bc.chooseSide (1);
			if (!facingUp) {
				FlipUp ();
			}
		} else if (vertical < 0) {
			bc.chooseSide (2);
			if (facingUp) {
				FlipUp ();
			}
		}

		if (horizontal > 0) {
			bc.chooseSide (3);
			if (!facingRight) {
				FlipSide ();
			}
		} else if (horizontal < 0) {
			bc.chooseSide (4);
			if (facingRight) {
				FlipSide ();
			}
		}

		if (life < 800) {
			img [4].enabled = false;
		} 
		if (life < 600) {
			img [3].enabled = false;
		} 
		if (life < 400) {
			img [2].enabled = false;
		} 
		if (life < 200) {
			img [1].enabled = false;
		} 
		if (life < 100) {
			img [0].enabled = false;
		}
		if (life < 0) {
			EndGame ();
		}
	}

	void OnTriggerStay2D (Collider2D other){
		if (other.name.Contains ("Zombie")) {
			//print (life);
			life--;
		}
	}

	void FlipUp (){
		facingUp = !facingUp;

		Vector3 theScale = transform.localScale;
		theScale.y *= -1;

		transform.localScale = theScale;
	}

	void FlipSide (){
		facingRight = !facingRight;

		Vector3 theScale = transform.localScale;
		theScale.x *= -1;

		transform.localScale = theScale;
	}

	public void EndGame (){

		SceneManager.LoadScene ("End");
	}
}
