﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletControl : MonoBehaviour {

	public PlayerController pc;

	public int power;
	public int side;

	// Use this for initialization
	void Start () {
		Destroy (gameObject, 0.8f);

	}
	
	// Update is called once per frame
	void Update () {

		if (side == 1) { //cima
			transform.Translate (0, power, 0);
		}

		else if (side == 2) { //baixo
			transform.Translate (0, power, 0);
		}

		else if (side == 3) { //direita
			transform.Translate ( power, 0, 0);
		}

		else if (side == 4) { //esquerda
			transform.Translate ( power, 0, 0);
		}
	}

	public void chooseSide (int i){
		side = i;
	}
}
